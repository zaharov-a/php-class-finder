<?php

namespace Zakharov\PhpClassFinder;

class ClassFinder
{
    public function find(string $filePath): ?string
    {
        return $this->findInText(file_get_contents($filePath));
    }

    public function findInText(string $text): ?string
    {
        $tokens    = token_get_all($text);
        $namespace = null;

        do {
            $token = current($tokens);
            switch (is_array($token) ? $token[0] : $token) {
                case T_NAMESPACE:
                    $take = [T_STRING, T_NS_SEPARATOR];
                    if (version_compare(PHP_VERSION, '8.0.0', '>=')) {
                        $take[] = T_NAME_QUALIFIED;
                        $take[] = T_NAME_FULLY_QUALIFIED;
                    }

                    $namespace = ltrim($this->fetch($tokens, $take) . '\\', '\\');
                    break;

                case T_CLASS:
                case T_INTERFACE:
                    if ($name = $this->fetch($tokens, [T_STRING])) {
                        return $namespace . $name;
                    }
                    break;
            }
        } while (next($tokens));

        return null;
    }

    private function fetch(&$tokens, $take): ?string
    {
        $res = null;
        while ($token = next($tokens)) {
            [$token, $s] = is_array($token) ? $token : [$token, $token];
            if (in_array($token, $take, true)) {
                $res .= $s;
            } elseif (!in_array($token, [T_DOC_COMMENT, T_WHITESPACE, T_COMMENT], true)) {
                break;
            }
        }

        return $res;
    }
}
