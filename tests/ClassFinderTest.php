<?php

namespace Zakharov\PhpClassFinder;

use PHPUnit\Framework\TestCase;

class ClassFinderTest extends TestCase
{

    public function testFindInText()
    {
        $finder = new ClassFinder();

        $this->assertNull($finder->findInText(''));
        $this->assertEquals('Test', $finder->findInText('<?php class Test{}'));
        $this->assertEquals('Test', $finder->findInText('<?php class Test{} class Test2{}'));
        $this->assertEquals(
            'TestNamespace\\Test\\Test',
            $finder->findInText('<?php namespace TestNamespace\\Test; class Test{}')
        );
        $this->assertEquals(
        'TestNamespace\\Test\\Test',
        $finder->findInText('<?php namespace TestNamespace\\Test; class Test{} class Test2{}')
    );
    }
}
